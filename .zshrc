# Path to your oh-my-zsh installation.

export ZSH="/home/tyler/.oh-my-zsh"

# My theme

ZSH_THEME="robbyrussell"

# My plugins

plugins=(
	git 
	zsh-syntax-highlighting 
	golang
	zsh-autosuggestions
)

# Check for update and initialize ohmyzsh

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment

export LC_ALL="en_US.UTF-8"

# Swap caps and esc

setxkbmap -option caps:swapescape

# Fuzzy search files best thing I have added to my shell ever . 

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Alias

alias scp='noglob scp'
alias rm='rm -iv'
alias grep='grep --color=auto'
alias ls='ls --color=auto'

# Path

export PATH=/usr/local/go/bin:$PATH #GO BINARIES
export PATH="$HOME/Android/tools:$PATH" 
export PATH="$HOME/Android/platform-tools:$PATH"

# Other env variables

export GOROOT=/usr/local/go #GOROOT
export GOPATH=$HOME/go/ #GOPATH 
export JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64/
export ANDROID_HOME=/usr/lib/android-sdk
