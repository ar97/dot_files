" 18/06/2020

" Plugins
call plug#begin()
Plug 'w0rp/ale'
Plug 'preservim/NERDTree'
Plug 'mattn/emmet-vim'
call plug#end()

" Other 
set number
set nuw=4
set linespace=4

" Bring up NERDTree at start and all things NERDTree
au VimEnter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeStatusline = 'File Browser'

" Close everything if NERDTree is the last window open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Handle colors
colorscheme slate
